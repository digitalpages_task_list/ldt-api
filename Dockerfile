FROM node:12.14.1

WORKDIR /app

COPY . /app

EXPOSE 3333

CMD ["npm", "start"]

#docker build -t my_node .
#docker run -p 3333:3333 -d my_node