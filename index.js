const express = require('express');
const cors = require('cors');
const router = require('./routes')
const bodyParser = require('body-parser')
const app = express();

app.use(cors())

app.get('/', function (req, res) {
    res.send('Hello Digital Pages!');
});

// app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())
app.use('/api/v1', router)

app.listen(3333, function () {
    console.log('app listening on port 3333!');
});