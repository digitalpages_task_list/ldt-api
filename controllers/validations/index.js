module.exports = {

    validate(req, obj){
        let errors = []
        for(let field in obj){
            let error = this[obj[field]](req, field)
            if(error)
                errors.push(error)
        }
        if(errors.length == 0)
            return null

        return errors
    },

    required(req, field){
        if(!req.body[field])
            return `O campo ${field} é obrigatório!`
    },


}