const { Tarefa } = require('../models');
const helpers = require('./helpers');
const validator = require('./validations');
module.exports = {

    async index(req, res){
        const resp = await Tarefa.findAll({where: req.query})
        return res.json(resp)
    },

    async show(req, res){
        const resp = await Tarefa.findByPk(req.params.id)
        return res.json(resp)
    },

    async create(req, res){
        const fails = validator.validate(req,{titulo: 'required'})
        if(fails)
            return res.status(422).send(fails)

        const resp = await Tarefa.create(req.body)
        return res.json(resp)
    },

    async update(req, res){
        let model = await Tarefa.findByPk(req.params.id)
        if(!model)
            return res.status(404).send(`id ${req.params.id} não encontrado`)

        model = helpers.fill(model, req.body)
        return res.json(await model.save())
    },

    async delete(req, res){
        const resp = await Tarefa.destroy({where:{id: req.params.id}})
        return res.json(resp)
    },

}