module.exports = {
    fill(model, requestBody){
        for(let values in requestBody)
            model[values] = requestBody[values]
        return model
    }
}