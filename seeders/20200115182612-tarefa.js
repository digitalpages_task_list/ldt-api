'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Tarefas', [
      {
        titulo: 'John',
        descricao: 'Doe',
        responsavel: 'Loren',
        data_de_entrega: '2020-01-15',
        status: 'Ipsun',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        titulo: 'Loren',
        descricao: 'John',
        responsavel: 'Doe',
        data_de_entrega: '2020-01-16',
        status: 'Ipsun',
        createdAt: new Date(),
        updatedAt: new Date()
      },

    ], {});

  },

  down: (queryInterface, Sequelize) => {

    return queryInterface.bulkDelete('Tarefas', null, {});

  }
};
