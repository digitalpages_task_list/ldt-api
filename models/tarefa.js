'use strict';
module.exports = (sequelize, DataTypes) => {
  const Tarefa = sequelize.define('Tarefa', {
    titulo: DataTypes.STRING,
    descricao: DataTypes.STRING,
    responsavel: DataTypes.STRING,
    data_de_entrega: DataTypes.DATEONLY,
    status: DataTypes.STRING
  }, {});
  Tarefa.associate = function(models) {
    // associations can be defined here
  };
  return Tarefa;
};