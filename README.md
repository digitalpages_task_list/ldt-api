##Iniciando aplicação através do docker.

Na pasta raís do projeto...

Instale as dependências (Node precisa estar instalado). 
### `npm install`

Execute o comando para subir os containers.
### `docker-compose up -d`

Crie as tabelas e colunas necessárias.
### `docker-compose exec node bash -c "npx sequelize-cli db:migrate"`

(Opcional) Semeie as tabelas com dados.
### `docker-compose exec node bash -c "npx sequelize-cli db:seed:all"`


Se não houverem alterações nos arquivos do docker, tente acessar o seguinte link:
[http://localhost:3333/api/v1/tarefas](http://localhost:3333/api/v1/tarefas)

Se visualizar uma lista de tarefas, está tudo certo!

 