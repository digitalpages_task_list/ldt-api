const express = require('express');

const TarefasController = require('./controllers/TarefasController')

router = express.Router()

router.get('/tarefas', TarefasController.index)
router.get('/tarefas/:id', TarefasController.show)
router.post('/tarefas', TarefasController.create)
router.put('/tarefas/:id', TarefasController.update)
router.delete('/tarefas/:id', TarefasController.delete)

module.exports = router